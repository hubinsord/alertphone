package com.example.alertphone.data.main.model

data class NotificationData(
    val title: String,
    val message: String
)