package com.example.alertphone.data.main.model

data class PushNotification(
    val data: NotificationData,
    val to: String
)